using ShareToSave.BusinessLogic;
using ShareToSave.Repositories;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace ShareToSave
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType<IAdminManagement, AdminManagement>();//written by me to intialize an object whenever i call interface
            container.RegisterType<IAdminRepository, AdminRepository>();
           
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}