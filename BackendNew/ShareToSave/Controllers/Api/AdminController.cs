﻿
using System.Web.Http;
using ShareToSave.Models;
using ShareToSave.BusinessLogic;

namespace ShareToSave.Controllers
{
    public class AdminController : ApiController
    {
        private IAdminManagement adminManagement;

        public AdminController(IAdminManagement adminManagement)
        {
            this.adminManagement = adminManagement;
        }


        [HttpGet]
        [Route("api/Admin/GetAll")]
        public IHttpActionResult GetAll()
        {
            var getall = adminManagement.GetAll();
            return Ok(getall);
        }


        [Route("api/Admin/GetById")]
        [HttpGet]
        public IHttpActionResult GetById(int id)
        {
            Admin emp = null;
            emp = adminManagement.GetById(id);
            return Ok(emp);
        }

        //  [HttpPost]
        public Admin Post([FromBody]Admin admin)
        {
            return adminManagement.Post(admin);

        }

        [HttpPut]
        [Route("api/PutAdmin")]
        public void Put([FromBody]Admin admin)
        {
            adminManagement.Put(admin);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            adminManagement.Delete(id);
            return Ok();
        }

    }
}