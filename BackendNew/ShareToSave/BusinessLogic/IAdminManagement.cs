﻿using ShareToSave.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareToSave.BusinessLogic
{
    public interface IAdminManagement
    {
        IEnumerable<Admin> GetAll();
        Admin Post(Admin admin);
        void Put(Admin admin);
        void Delete(int id);
        Admin GetById(int Id);
    }
}
