﻿using ShareToSave.Models;
using ShareToSave.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShareToSave.BusinessLogic
{
    public class AdminManagement : IAdminManagement
    {
        private IAdminRepository adminRepository;

        public AdminManagement(IAdminRepository adminRepository)
        {
            this.adminRepository = adminRepository;
        }

        public IEnumerable<Admin> GetAll()
        {
            var getall = adminRepository.GetAll();
            return getall;
        }

        public Admin GetById(int id)
        {
            Admin emp = null;
            emp = adminRepository.GetById(id);
            return emp;
        }

        public Admin Post(Admin admin)
        {
            return adminRepository.Post(admin);
        }

        public void Put(Admin admin)
        {
            adminRepository.Put(admin);
        }

        public void Delete(int id)
        {
            adminRepository.Delete(id);
        }
    }
}