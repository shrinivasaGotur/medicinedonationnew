﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;
using System.Net.Http;
using ShareToSave.Models;

namespace ShareToSave.Repositories
{
    public interface IAdminRepository
    {
        IEnumerable<Admin> GetAll();
        Admin Post(Admin admin);
        void Put(Admin admin);
        void Delete(int id);
        Admin GetById(int Id);
    }
}

