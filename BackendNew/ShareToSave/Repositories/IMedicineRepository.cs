﻿using ShareToSave.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareToSave.Repositories
{
    interface IMedicineRepository
    {
        IEnumerable<Medicine> GetAllMedicines();
        void PostMedicine(Medicine medicine);
        void UpdateMedicine(Medicine medicine);
        List<string> GetMedicineByName(string medicineName);
    }
}
