﻿using ShareToSave.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShareToSave.Repositories
{
    public class AdminRepository : IAdminRepository
    {
        EntityData dbentities = new EntityData();

        public IEnumerable<Admin> GetAll()
        {
            var allAdmins = dbentities.Admins.ToList();
            return allAdmins;
        }

        public Admin GetById(int Id)
        {
            Admin admin = null;
            using (var context = new EntityData())
            {
                admin = context.Admins.FirstOrDefault(e => e.AdminId == e.AdminId);
            }
            return admin;
        }

        public Admin Post(Admin admin)
        {
            using (EntityData dbentities = new EntityData())
            {
                dbentities.Admins.Add(admin);
                dbentities.SaveChanges();
                return admin;
            }
        }

        public void Put(Admin admin)
        {
            using (EntityData dbentities = new EntityData())
            {
                var entity = dbentities.Admins.FirstOrDefault(e => e.AdminId == e.AdminId);
                var email = admin.Email;
                var UserEmail = (from x in dbentities.Admins where x.Email == email select x).ToList();

                foreach (var u in UserEmail)
                {
                    dbentities.Admins.Remove(u);
                }
                entity.Name = admin.Name;
                entity.Email = admin.Email;
                entity.MobileNumber = admin.MobileNumber;
                entity.PassCode = admin.PassCode;
                entity.Address = admin.Address;
                entity.Password = admin.Password;
                entity.GovID = admin.GovID;

                dbentities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                dbentities.SaveChanges();

            }
        }

        public void Delete(int id)
        {
            using (var ctx = new EntityData())
            {
                var employee = ctx.Admins.Where(s => s.AdminId == id).FirstOrDefault();

                ctx.Entry(employee).State = System.Data.Entity.EntityState.Deleted;
                ctx.SaveChanges();
            }
        }
    }

}